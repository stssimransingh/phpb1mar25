<?php 
    $connect = new mysqli("localhost","root","","phpb1");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class=container>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Fullname</th>
                            <th>Username</th>
                            <th>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $qry = "SELECT * FROM users";
                            $result = $connect->query($qry);
                            while($row = $result->fetch_assoc()){
                                echo "<tr>";
                                
                                echo "<td>".$row['name']."</td>";
                                echo "<td>".$row['username']."</td>";
                                echo "<td>
                                    <a href='delete.php?id=".$row['id']."' class='btn btn-danger'>Delete</a>
                                    <a href='edit.php?id=".$row['id']."' class='btn btn-warning'>Edit</a>
                                    </td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>